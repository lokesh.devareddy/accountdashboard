import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
import * as ReactBootstrap from 'react-bootstrap';

const Radio = ReactBootstrap.Radio,
      FormGroup = ReactBootstrap.FormGroup,
      ControlLabel = ReactBootstrap.ControlLabel,
      FormControl = ReactBootstrap.FormControl,
      Button = ReactBootstrap.Button,
      HelpBlock = ReactBootstrap.HelpBlock,
      Checkbox = ReactBootstrap.Checkbox;

class ExpensesForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      name:"",
      category:""
    }
  }

  getJSONData(){
    const jsonData = {}
    for (var ref in this.refs) {
      jsonData[ref] =ReactDOM.findDOMNode(this.refs[ref]).value
    }
    return jsonData
  }

  handleSubmit(event){
    event.preventDefault()
    console.log(this.getJSONData())
  }

  render() {
    function FieldGroup({ id, label, help, ...props }) {
      return (
        <FormGroup controlId={id} style={{display:'flex'}}>
          <ControlLabel style={{width:'300px',textAlign:'left',marginLeft:'10px'}}>{label}</ControlLabel>
          <FormControl {...props} />
          {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
      );
    }
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <FormGroup controlId="formControlsText" style={{display:'flex'}}>
          <ControlLabel style={{width:'300px',textAlign:'left',marginLeft:'10px'}}>Name</ControlLabel>
          <FormControl type="text"  ref="name" placeholder="Enter Name"/>
          <ControlLabel style={{width:'300px',textAlign:'left',marginLeft:'10px'}}>Mobile</ControlLabel>
          <FormControl type="text"  ref="mobile" placeholder="Enter Mobile"/>
        </FormGroup>
        <FormGroup controlId="formControlsText" style={{display:'flex'}}>
          <ControlLabel style={{width:'300px',textAlign:'left',marginLeft:'10px'}}>From</ControlLabel>
          <FormControl componentClass="select" ref="from" placeholder="select">
            <option ></option>
            <option value="chennai">Chennai</option>
            <option value="madurai">Madurai</option>
            <option value="theni">Theni</option>
            <option value="dindugal">Dindugal</option>
            <option value="pondicherry">Pondicherry</option>
            <option value="salem">Salem</option>
          </FormControl>
          <ControlLabel style={{width:'300px',textAlign:'left',marginLeft:'10px'}}>To</ControlLabel>
          <FormControl componentClass="select" ref="to" placeholder="select">
            <option></option>
            <option value="chennai">Chennai</option>
            <option value="madurai">Madurai</option>
            <option value="theni">Theni</option>
            <option value="dindugal">Dindugal</option>
            <option value="pondicherry">Pondicherry</option>
            <option value="salem">Salem</option>
          </FormControl>
        </FormGroup>
        <FormGroup controlId="formControlsText" style={{display:'flex'}}>
        <ControlLabel style={{width:'300px',textAlign:'left',marginLeft:'10px'}}>Category</ControlLabel>
          <FormControl componentClass="select" ref="category" placeholder="select">
            <option ></option>
            <option value="groceries">Groceries</option>
            <option value="bike">Bike</option>
            <option value="electronics">Electronics</option>
            <option value="medicines">Medicine</option>
          </FormControl>          
          <ControlLabel style={{width:'300px',textAlign:'left',marginLeft:'10px'}}>Quantity</ControlLabel>
          <FormControl type="text"/>
        </FormGroup>

    <FormGroup bsSize="small" style={{display:'flex'}}>
          <ControlLabel style={{width:'300px',textAlign:'left',marginLeft:'10px'}}>Amount</ControlLabel>
          <FormControl bsSize="small" type="text" ref="amount"/>
    </FormGroup>

    <Button type="submit">Submit</Button>
  </form>
    );
  }
}

export default ExpensesForm;
