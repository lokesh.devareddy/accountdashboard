import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ExpenseForm from './ExpensesForm'

class App extends Component {
  render() {
    return (
      <div className="App">
        <ExpenseForm/>
      </div>
    );
  }
}

export default App;
